<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://lukasgrasse.com
 * @since      1.0.0
 *
 * @package    Simple_Globe
 * @subpackage Simple_Globe/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Simple_Globe
 * @subpackage Simple_Globe/admin
 * @author     Lukas Grasse <lukasgrasse@gmail.com>
 */
class Simple_Globe_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $simple_globe    The ID of this plugin.
	 */
	private $simple_globe;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $simple_globe       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $simple_globe, $version ) {

		$this->simple_globe = $simple_globe;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Simple_Globe_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Simple_Globe_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->simple_globe, plugin_dir_url( __FILE__ ) . 'css/simple-globe-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Simple_Globe_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Simple_Globe_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->simple_globe, plugin_dir_url( __FILE__ ) . 'js/simple-globe-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function settings_menu() {
		add_menu_page( 'Simple Globe Settings', 'Simple Globe', 'manage_options', 'simple-globe-settings', array( $this, 'settings_menu_options' ) );
	}


	public function settings_menu_options() {
 
    $globe_oceans_color = (get_option('globe_oceans_color') != '') ? get_option('globe_oceans_color') : '#000080';
    $globe_borders_color = (get_option('globe_borders_color') != '') ? get_option('globe_borders_color') : '#008000';
    $globe_land_color = (get_option('globe_land_color') != '') ? get_option('globe_land_color') : '#339966';
 
    $globe_rotate  = (get_option('globe_rotate') == 'enabled') ? 'checked' : '' ;
 

 
    $html = '</pre>
			<div class="wrap"><form action="options.php" method="post" name="options">
			<h2>Simple Globe Settings</h2>
			' . wp_nonce_field('update-options') . '
			<table class="form-table" width="100%" cellpadding="10">
			<tbody>

	
			<tr>
			<td scope="row" align="left">
			 <label>Ocean Color  </label><input type="text" name="globe_oceans_color" value="' . $globe_oceans_color . '" /></td>
			</tr>

			<tr>
			<td scope="row" align="left">
			 <label>Land Color   </label><input type="text" name="globe_land_color" value="' . $globe_land_color . '" /></td>
			</tr>

			<tr>
			<td scope="row" align="left">
			 <label>Border Color </label><input type="text" name="globe_borders_color" value="' . $globe_borders_color . '" /></td>
			</tr>

			<tr>
			<td scope="row" align="left">
			 <label>Enable Rotation </label><input type="checkbox" name="globe_rotate" value="enabled" ' . $globe_rotate . '/></td>
			</tr>

			</tbody>
			</table>
			 <input type="hidden" name="action" value="update" />
			 
			 <input type="hidden" name="page_options" value="globe_oceans_color, globe_land_color, globe_borders_color, globe_rotate" />
			 
			 <input type="submit" name="Submit" value="Update" /></form></div>
			<pre>';
 
    echo $html;
	}



}
