<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://lukasgrasse.com
 * @since      1.0.0
 *
 * @package    Simple_Globe
 * @subpackage Simple_Globe/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Simple_Globe
 * @subpackage Simple_Globe/public
 * @author     Lukas Grasse <lukasgrasse@gmail.com>
 */
class Simple_Globe_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $simple_globe    The ID of this plugin.
	 */
	private $simple_globe;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $simple_globe       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $simple_globe, $version ) {

		$this->simple_globe = $simple_globe;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Simple_Globe_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Simple_Globe_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->simple_globe, plugin_dir_url( __FILE__ ) . 'css/simple-globe-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Simple_Globe_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Simple_Globe_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( 'd3', 'http://d3js.org/d3.v3.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'd3topo', 'http://d3js.org/topojson.v1.min.js', array( 'jquery', 'd3' ), $this->version, false );
		wp_enqueue_script( 'planetaryjs', plugin_dir_url( __FILE__ ) . 'js/planetaryjs.min.js', array( 'jquery', 'd3', 'd3topo' ), $this->version, false );
		wp_enqueue_script( $this->simple_globe, plugin_dir_url( __FILE__ ) . 'js/simple-globe-public.js', array( 'jquery', 'd3', 'd3topo', 'planetaryjs' ), $this->version, false );
		
		$globe_oceans_color = (get_option('globe_oceans_color') != '') ? get_option('globe_oceans_color') : '#000080';
	    $globe_borders_color = (get_option('globe_borders_color') != '') ? get_option('globe_borders_color') : '#008000';
	    $globe_land_color = (get_option('globe_land_color') != '') ? get_option('globe_land_color') : '#339966';
	 
	    $globe_rotate  = (get_option('globe_rotate') == 'enabled') ? true : false ;
	 



		$globe_settings = array(
			'url' => plugin_dir_url( __FILE__ ),
			'globe_oceans_color'  => $globe_oceans_color,
			'globe_land_color'    => $globe_land_color,
			'globe_borders_color' => $globe_borders_color,
			'globe_rotate'        => $globe_rotate
		);


		wp_localize_script($this->simple_globe, 'script_vars', $globe_settings);

		function globe_func( $atts ){
			return '<canvas id="rotatingGlobe" style="width: 600px; height: 600px; cursor: move;" width="600" height="600"></canvas>';
		}
		add_shortcode( 'simple_globe', 'globe_func' );
	}

}
