(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */
jQuery( document ).ready(function() {

  var globe1 = planetaryjs.planet();
  
  renderGlobe(globe1, 'rotatingGlobe');
  

});

})( jQuery );

function renderGlobe(globe, globeID) {
  // Load our custom `autorotate` plugin; see below.
  if(script_vars.globe_rotate)
  globe.loadPlugin(autorotate(10));
  // The `earth` plugin draws the oceans and the land; it's actually
  // a combination of several separate built-in plugins.
  //
  // Note that we're loading a special TopoJSON file
  // (world-110m-withlakes.json) so we can render lakes.
  globe.loadPlugin(planetaryjs.plugins.earth({
    topojson: { file:   script_vars.url + '/js/data/world-110m.json' },
    oceans:   { fill:   script_vars.globe_oceans_color },
    land:     { fill:   script_vars.globe_land_color },
    borders:  { stroke: script_vars.globe_borders_color }
  }));

  // The `pings` plugin draws animated pings on the globe.
  globe.loadPlugin(planetaryjs.plugins.markers());
  // The `zoom` and `drag` plugins enable
  // manipulating the globe with the mouse.
  globe.loadPlugin(planetaryjs.plugins.zoom({
    scaleExtent: [100, 800]
  }));
  globe.loadPlugin(planetaryjs.plugins.drag({
    // Dragging the globe should pause the
    // automatic rotation until we release the mouse.
    onDragStart: function() {
      this.plugins.autorotate.pause();
    },
    onDragEnd: function() {
     if(script_vars.globe_rotate)
     this.plugins.autorotate.resume();
    }
  }));
  // Set up the globe's initial scale, offset, and rotation.
  globe.projection.scale(300).translate([300, 300]).rotate([0, -10, 0]);

 
 // setInterval(function() {
    function addRandomMarker() {
    var lat = Math.random() * 170 - 85;
    var lng = Math.random() * 360 - 180;
    var color = 'yellow';
    globe.plugins.markers.add(lng, lat, { color: color, ttl: 2000, angle: Math.random() * 10 });
  //}, 1500);
  };

  var canvas = document.getElementById(globeID);

   if(!canvas) {
    return;
   }
  // Special code to handle high-density displays (e.g. retina, some phones)
  // In the future, Planetary.js will handle this by itself (or via a plugin).
  if (window.devicePixelRatio == 2) {
    canvas.width = 800;
    canvas.height = 800;
    context = canvas.getContext('2d');
    context.scale(2, 2);
  }
  // Draw that globe!
    globe.draw(canvas);

  for(var i=0; i<5; i++) {
    addRandomMarker();
  }
  
};

  // This plugin will automatically rotate the globe around its vertical
  // axis a configured number of degrees every second.
  function autorotate(degPerSec) {
    // Planetary.js plugins are functions that take a `planet` instance
    // as an argument...
    return function(planet) {
      var lastTick = null;
      var paused = false;
      planet.plugins.autorotate = {
        pause:  function() { paused = true;  },
        resume: function() { paused = false; }
      };
      // ...and configure hooks into certain pieces of its lifecycle.
      planet.onDraw(function() {
        if (paused || !lastTick) {
          lastTick = new Date();
        } else {
          var now = new Date();
          var delta = now - lastTick;
          // This plugin uses the built-in projection (provided by D3)
          // to rotate the globe each time we draw it.
          var rotation = planet.projection.rotate();
          rotation[0] += degPerSec * delta / 1000;
          if (rotation[0] >= 180) rotation[0] -= 360;
          planet.projection.rotate(rotation);
          lastTick = now;
        }
      });
    };
  };


  planetaryjs.plugins.markers = function(config) {
    var markers = [];
    config = config || {};

    var addMarker = function(lng, lat, options) {
      options = options || {};
      options.color = options.color || config.color || 'white';
      options.angle = options.angle || config.angle || 5;
      options.ttl   = options.ttl   || config.ttl   || 2000;
      var marker = { time: new Date(), options: options };
      if (config.latitudeFirst) {
        marker.lat = lng;
        marker.lng = lat;
      } else {
        marker.lng = lng;
        marker.lat = lat;
      }
      markers.push(marker);
    };

    var drawMarkers = function(planet, context, now) {
      var newMarkers = [];
      for (var i = 0; i < markers.length; i++) {
        var marker = markers[i];
        var alive = now - marker.time;
 //       if (alive < marker.options.ttl) {
          newMarkers.push(marker);
          drawMarker(planet, context, now, alive, marker);
//        }
      }
      markers = newMarkers;
    };

    var drawMarker = function(planet, context, now, alive, marker) {
      var alpha = 1;
      var color = d3.rgb(marker.options.color);
      color = "rgba(" + color.r + "," + color.g + "," + color.b + "," + alpha + ")";
      context.strokeStyle = color;
      var circle = d3.geo.circle().origin([marker.lng, marker.lat])
       .angle(1.1)();
      context.beginPath();
      planet.path.context(context)(circle);
      context.fillStyle=color;
      context.strokeStyle='black';
      context.lineWidth=2;
      context.fill();
      context.stroke();


    };

    return function (planet) {
      planet.plugins.markers = {
        add: addMarker
      };

      planet.onDraw(function() {
        var now = new Date();
        planet.withSavedContext(function(context) {
          drawMarkers(planet, context, now);
        });
      });
    };
  };