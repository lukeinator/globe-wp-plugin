<?php

/**
 * Fired during plugin activation
 *
 * @link       http://lukasgrasse.com
 * @since      1.0.0
 *
 * @package    Simple_Globe
 * @subpackage Simple_Globe/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Simple_Globe
 * @subpackage Simple_Globe/includes
 * @author     Lukas Grasse <lukasgrasse@gmail.com>
 */
class Simple_Globe_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
