<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://lukasgrasse.com
 * @since      1.0.0
 *
 * @package    Simple_Globe
 * @subpackage Simple_Globe/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Simple_Globe
 * @subpackage Simple_Globe/includes
 * @author     Lukas Grasse <lukasgrasse@gmail.com>
 */
class Simple_Globe_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
